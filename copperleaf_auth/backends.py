from social.backends.oauth import BaseOAuth2

from six.moves.urllib.parse import urljoin

class CopperLeafOAuth2(BaseOAuth2):
    name = 'copperleaf'
    ACCESS_TOKEN_METHOD = 'POST'

    def authorization_url(self):
        return self.setting('IDENTITY_SERVICE_AUTHORIZATION_URL')

    def access_token_url(self):
        return self.setting('IDENTITY_SERVICE_ACCESS_TOKEN_URL')

    def api_url(self):
        return self.setting('IDENTITY_SERVICE_API_URL')

    def get_user_details(self, response):
        data = self.user_data(response['access_token'])
        data.update({'username': data['id']})
        return data

    def user_data(self, access_token, *args, **kwargs):
        return self.get_json('{0}/user'.format(self.api_url()), headers={'Authorization': 'Bearer {0}'.format(access_token)})
