from django.contrib.auth.models import Group


def save_groups(backend, user, details, *args, **kwargs):
    changed = False

    if 'groups' in details:
        for group_name in details['groups']:
            group = Group.objects.get(name=group_name)
            if not user.groups.filter(id=group.id).exists():
                user.groups.add(group)
                changed = True

    if changed:
        backend.strategy.storage.user.changed(user)
