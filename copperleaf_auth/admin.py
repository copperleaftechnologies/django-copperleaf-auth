from django.contrib import admin
from .models import User


class UserAdmin(admin.ModelAdmin):
    fields = ('email', 'first_name', 'last_name', 'last_login', 'date_joined', 'groups', 'is_superuser',)
    readonly_fields = ('last_login', 'date_joined', 'groups', 'is_superuser',)

    def get_queryset(self, request, **kwargs):
        qs = super(UserAdmin, self).get_queryset(request)
        if not request.user.is_superuser:
            return qs.filter(organization_id=request.user.organization_id)
        else:
            return qs

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None, **kwargs):
        if obj and not request.user.is_superuser:
            return obj.organization_id == request.user.organization_id
        else:
            return super(UserAdmin, self).has_change_permission(request, **kwargs)

    def has_delete_permission(self, request, obj=None, **kwargs):
        if obj and not request.user.is_superuser:
            return obj.organization_id == request.user.organization_id
        else:
            return super(UserAdmin, self).has_delete_permission(request, **kwargs)

admin.site.register(User, UserAdmin)
