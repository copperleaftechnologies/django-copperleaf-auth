from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class CopperleafAuthConfig(AppConfig):
    name = 'copperleaf_auth'
    verbose_name = 'Copperleaf Authentication'
