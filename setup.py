import os
from setuptools import setup, find_packages

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name="django-copperleaf-auth",
    version="0.1",
    packages=find_packages(),
    include_package_data=True,
    description='A simple Django app to authenticate against the Copperleaf OAuth2 provider',
    long_description=README,
    author='Maxime Perreault',
    author_email='mperreault@copperleaf.com',
)
