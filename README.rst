===============
Copperleaf Auth
===============

Copperleaf Auth is a simple to facilitate authenticating your application against the Copperleaf OAuth2 provider

Quick start
-----------

0. Python Social Auth is a dependency and must be installed and configured first. See http://python-social-auth.readthedocs.org/en/latest/configuration/django.html

1. Add 'copperleaf_auth' and 'social.apps.django_app.default' to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'copperleaf_auth',
        ...
    ]

2. In your urls.py file, wrap the admin login view with a login_required decorator to ensure users go to site-wide login::

    from django.contrib.auth.decorators import login_required
    from django.contrib import admin

    admin.site.login = login_required(admin.site.login)

3. Add 'copperleaf_auth.backends.CopperLeafOAuth2' to your AUTHENTICATION_BACKENDS::

    AUTHENTICATION_BACKENDS = (
        ...
        'copperleaf_auth.backends.CopperLeafOAuth2',
        ...
    )

4. In settings, override LOGIN_ULL so copperleaf is the main authentication method::

    LOGIN_URL = '/login/copperleaf/'

5. Configure Django to use our User model::

    AUTH_USER_MODEL = 'copperleaf_auth.User'
    SOCIAL_AUTH_USER_MODEL = 'copperleaf_auth.User'
    SOCIAL_AUTH_USERNAME_IS_FULL_EMAIL = True

6. Choose whatever namespace you want to use for Python Social Auth::

    SOCIAL_AUTH_URL_NAMESPACE = 'social'

7. Configure Python Social Auth pipeline like so::

    SOCIAL_AUTH_PIPELINE = (
        'social.pipeline.social_auth.social_details',
        'social.pipeline.social_auth.social_uid',
        'social.pipeline.social_auth.auth_allowed',
        'social.pipeline.social_auth.social_user',
        'social.pipeline.user.create_user',
        'social.pipeline.social_auth.associate_user',
        'social.pipeline.social_auth.load_extra_data',
        'social.pipeline.user.user_details',
    )

8. Specify where the user should be redirected after login page::

    SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/admin'

9. After registering your app with the identity provider, add your key and secret to your settings::

    SOCIAL_AUTH_COPPERLEAF_KEY = ...
    SOCIAL_AUTH_COPPERLEAF_SECRET =  ....

10. In settings, configure the identity service URLs::

    IDENTITY_SERVICE_AUTHORIZATION_URL = ...
    IDENTITY_SERVICE_ACCESS_TOKEN_URL = ...
    IDENTITY_SERVICE_API_URL = ...

11. Run `python manage.py migrate` to create the User model

